#!/usr/bin/env python
#coding=utf-8

import platform
import time
import fileinput
import subprocess
import os
import sys
from threading import Thread
import time
import tail
import shutil
import select
import tempfile
import ciparams
import asyncprocess

cocos_log_time = 0
gradle_log_time = 0

def print_cocos_tail(txt):
    global cocos_log_time
    if txt == b'\r\n':
        return
    cur_time = time.time()
    cur_str = 'cocos ==> {}'.format(txt)
    if cocos_log_time + 5 < cur_time or cur_str.find('error') >= 0 or cur_str.find('Error') >= 0 or cur_str.find('Exception') >= 0:
        cocos_log_time = cur_time
        print(cur_str, flush = True)

def print_gradle_tail(txt):
    global gradle_log_time
    if txt == b'\r\n':
        return
    cur_time = time.time()
    cur_str = 'gradle ==> {}'.format(txt)
    if gradle_log_time + 5 < cur_time or cur_str.find('error') >= 0 or cur_str.find('Error') >= 0 or cur_str.find('Exception') >= 0:
        gradle_log_time = cur_time
        print(cur_str, flush = True)
    
def cocos_command(cmd_logger, project_path, log_path):
    """
    call cocos process to build
    """
    ci_params = ciparams.CIParams()

    cocos_path = ci_params.cocos_exe_dir + '212' + r'/CocosCreator.exe'
    if ci_params.cocos_version_code != '0':
        cocos_path = ci_params.cocos_exe_dir + ci_params.cocos_version_code + r'/CocosCreator.exe'

    build_cmd = [cocos_path, '--path', project_path
                 , '--build', 'platform=android']

    print (' '.join(build_cmd), flush = True)

    if os.path.exists(log_path):
        os.remove(log_path)

    cocos_log_file = open(log_path, 'w+')
    cocos_log_file.close()

    cocos_log_tail = open(log_path, 'rb')
    cocos_log_tail.seek(0, 2)

    process = asyncprocess.AsyncProcess(build_cmd, cwd=project_path, logger = cmd_logger)
    process.start()
    
    while cmd_logger.loop:
        cmd_logger.print_once()
        has_line = True
        while has_line:
            has_line = False
            curr_position = cocos_log_tail.tell()
            log_line = cocos_log_tail.readline()
            if not log_line:
                cocos_log_tail.seek(curr_position)
            else:
                has_line = True
                print_cocos_tail(log_line)
        time.sleep(1)

    cocos_log_tail.close()

    process.communicate()

def build_apk(cmd_logger,log_path,file_name):
    ci_params = ciparams.CIParams()

    # android_proj_path = ci_params.root_dir + "/client/build/jsb-default/frameworks/runtime-src/proj.android-studio/"
    android_proj_path = ci_params.root_dir + "/client/build/jsb-link/frameworks/runtime-src/proj.android-studio/"

    build_cmd = [android_proj_path+'gradlew', 'clean','assembleRelease',
                 '-P', 'VERSION_CODE='+ci_params.client_version_code,
                 '-P', 'VERSION_NAME=' + ci_params.client_version,
                 '-P', 'OUT_PUT_DIR_PARA=' + ci_params.outPut_dir,
                 '-P', 'OUT_PUT_APK_FILE_NAME=' + file_name]

    build_cmd_join = ' '.join(build_cmd)
    print(build_cmd_join, flush=True)

    if os.path.exists(log_path):
        os.remove(log_path)

    gradle_log_file = open(log_path, 'w+')
    gradle_log_file.close()

    gradle_log_tail = open(log_path, 'rb')
    gradle_log_tail.seek(0, 2)

    process = asyncprocess.AsyncProcess(build_cmd_join, cwd=android_proj_path, logger=cmd_logger,isShell=True)
    process.start()

    while cmd_logger.loop:
        cmd_logger.print_once()
        has_line = True
        while has_line:
            has_line = False
            curr_position = gradle_log_tail.tell()
            log_line = gradle_log_tail.readline()
            if not log_line:
                gradle_log_tail.seek(curr_position)
            else:
                has_line = True
                print_gradle_tail(log_line)
        time.sleep(1)

    gradle_log_tail.close()

    process.communicate()

def check_app_create_exit(apk_dir, cmd_logger):
    if not os.path.exists(apk_dir):
        cmd_logger.stop()
        print(apk_dir + ' not exists')
        exit(1)

if __name__ == '__main__':
    ci_params = ciparams.CIParams()

    cmd_logger = tail.OutErrLogHelper(ci_params.log_dir + os.sep + 'build_cmd_' + ci_params.log_tag )
    if ci_params.ci_runner_tags == 'my-tag':
        out_dir = ci_params.outPut_dir
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        time_format = time.strftime('%m%d_%H%M',time.localtime(time.time()))
        apk_file_name = ci_params.apk_name
        apk_dir = ci_params.outPut_dir + os.sep + apk_file_name
        if os.path.exists(apk_dir):
            os.remove(apk_dir)

        if ci_params.build_apk_dev != 'false':
            cocos_command(cmd_logger, ci_params.root_dir + os.sep + 'client'
                ,  ci_params.log_dir + os.sep + 'cocos_build_dev_net_' + ci_params.log_tag + '.log')
            build_apk(cmd_logger, ci_params.log_dir + os.sep + 'gradle_build_dev_net_' + ci_params.log_tag + '.log',apk_file_name)
            check_app_create_exit(apk_dir, cmd_logger)

        if ci_params.build_apk_res != 'false':
            cocos_command(cmd_logger, ci_params.root_dir + os.sep + 'client'
                , ci_params.log_dir + os.sep + 'cocos_build_res_net_' + ci_params.log_tag + '.log')
            build_apk(cmd_logger, ci_params.log_dir + os.sep + 'gradle_build_res_net_' + ci_params.log_tag + '.log',apk_file_name)
            check_app_create_exit(apk_dir, cmd_logger)
            
    cmd_logger.stop()