import asyncprocess
import ciparams
import tail
import os
def build_apk():
    ci_params = ciparams.CIParams()
    cmd_logger = tail.OutErrLogHelper("D:/Code/joyport/yingluo/"+ 'build_cmd_' + ci_params.log_tag)
    android_proj_path = "D:/Code/joyport/yingluo/client/build/jsb-default/frameworks/runtime-src/proj.android-studio/"

    build_cmd = [android_proj_path+'gradlew', 'clean','assembleRelease']

    build_cmd_join = ' '.join(build_cmd);
    print(build_cmd_join, flush=True)

    process = asyncprocess.AsyncProcess(android_proj_path+"gradlew.bat clean assembleRelease", cwd=android_proj_path, logger=cmd_logger,isShell=True)
    process.start()

    process.communicate()

if __name__ == '__main__':
    build_apk()