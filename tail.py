#!/usr/bin/env python
#coding=utf-8

'''
Python-Tail - Unix tail follow implementation in Python. 
python-tail can be used to monitor changes to a file.
Example:
    import tail
    # Create a tail instance
    t = tail.Tail('file-to-be-followed')
    # Register a callback function to be called when a new line is found in the followed file. 
    # If no callback function is registerd, new lines would be printed to standard out.
    t.register_callback(callback_function)
    # Follow the file with 5 seconds as sleep time between iterations. 
    # If sleep time is not provided 1 second is used as the default time.
    t.follow(s=5) '''

import os
import sys
import time
from threading import Thread

class Tail(object):
    ''' Represents a tail command. '''
    def __init__(self, tailed_file):
        ''' Initiate a Tail instance.
            Check for file validity, assigns callback function to standard out.
            
            Arguments:
                tailed_file - File to be followed. '''

        self.check_file_validity(tailed_file)
        self.tailed_file = tailed_file
        self.callback = sys.stdout.write
        self.loop = True
        self.loop_left_count = 0

    def follow(self, s=1):
        ''' Do a tail follow. If a callback function is registered it is called with every new line. 
        Else printed to standard out.
    
        Arguments:
            s - Number of seconds to wait between each iteration; Defaults to 1. '''

        with open(self.tailed_file, 'rb') as file_:
            # Go to the end of file
            file_.seek(0,2)
            while self.loop:
                curr_position = file_.tell()
                line = file_.readline()
                if not line:
                    file_.seek(curr_position)
                    time.sleep(s)
                else:
                    self.callback(line)
                if self.loop_left_count > 0:
                    self.loop_left_count = self.loop_left_count - 1
                    self.loop = self.loop_left_count > 0
            file_.close()

    def register_callback(self, func):
        ''' Overrides default callback function to provided function. '''
        self.callback = func

    def check_file_validity(self, file_):
        ''' Check whether the a given file exists, readable and is a file '''
        if not os.access(file_, os.F_OK):
            raise TailError("File '%s' does not exist" % (file_))
        if not os.access(file_, os.R_OK):
            raise TailError("File '%s' not readable" % (file_))
        if os.path.isdir(file_):
            raise TailError("File '%s' is a directory" % (file_))

    def stop(self, wait_count = 0):
        self.loop_left_count = wait_count
        self.loop = self.loop_left_count > 0

class TailError(Exception):
    def __init__(self, msg):
        self.message = msg
    def __str__(self):
        return self.message

class OutErrLogHelper(object):
    def __init__(self, log_file):
        out_log_path = log_file + '_out.log'
        err_log_path = log_file + '_err.log'
        if os.path.exists(out_log_path):
            os.remove(out_log_path)
        if os.path.exists(err_log_path):
            os.remove(err_log_path)
        self.out_log_file = open(out_log_path, 'w+')
        self.err_log_file = open(err_log_path, 'w+')

        self.out_tail_file = open(out_log_path, 'rb')
        self.err_tail_file = open(err_log_path, 'rb')
        self.out_tail_file.seek(0,2)
        self.err_tail_file.seek(0,2)
        self.loop = True
        self.loop_left_count = 0
        self.last_log_time = 0
    
    def print_once(self):
        has_line = True
        while has_line:
            has_line = False
            curr_position = self.out_tail_file.tell()
            line = self.out_tail_file.readline()
            if not line:
                self.out_tail_file.seek(curr_position)
            else:
                has_line = True
                self.print_out_tail(line)

            curr_position = self.err_tail_file.tell()
            line = self.err_tail_file.readline()
            if not line:
                self.err_tail_file.seek(curr_position)
            else:
                has_line = True
                self.print_err_tail(line)

        if self.loop_left_count > 0:
            self.loop_left_count = self.loop_left_count - 1
            self.loop = self.loop_left_count > 0

    def print_while(self, s = 1):
        while self.loop:
            self.print_once()
            time.sleep(s)

    def print_out_tail(self, txt):
        if txt == b'\r\n':
            return
        cur_time = time.time()
        cur_str = 'out ==> {}'.format(txt)
        if cur_time >= self.last_log_time + 5  or cur_str.find('error') >= 0 or cur_str.find('Error') >= 0:
            print(cur_str, flush = True)
            self.last_log_time = cur_time

    def print_err_tail(self, txt):
        if txt == b'\r\n':
            return
        print('err ==> {}'.format(txt), flush = True)
        self.last_log_time = time.time()

    def stop(self, wait_count = 5):
        self.loop_left_count = wait_count
        self.loop = self.loop_left_count > 0
        self.out_log_file.close()
        self.err_log_file.close()
        self.out_tail_file.close()
        self.err_tail_file.close()