#!/usr/bin/env python
#coding=utf-8

import platform
import time
import fileinput
import subprocess
import os
import sys
from threading import Thread
import time
import tail
import shutil
import select
import tempfile
import ciparams

class AsyncProcess(object):
    def __init__(self, args, logger, cwd=None,isShell = False):
        self.process_t = None
        self.process = None
        self.args = args
        self.cwd = cwd
        self.logger = logger
        self.process_out = None
        self.process_err = None
        self.is_shell = isShell

    def thread_call(self):
        self.logger.loop = True
        self.logger.loop_left_count = 0

        self.process = subprocess.Popen(self.args, shell=self.is_shell,cwd=self.cwd, stdout=self.logger.out_log_file,
                                        stderr=self.logger.err_log_file)
        (out_d, err_d) = self.process.communicate()
        self.process_out = out_d
        self.process_err = err_d
        self.logger.loop_left_count = 2

    def start(self):
        self.process_t = Thread(target=self.thread_call)
        self.process_t.start()

    def communicate(self):
        #(out_d, err_d) = self.process.communicate()
        self.process_t.join()
        if self.process_out:
            print('process_out ==> {}'.format(self.process_out), flush = True)
        if self.process_err:
            print('process_err ==> {}'.format(self.process_err), flush = True)
        if self.process.returncode != 0:
            self.logger.stop()
            raise Exception('command error')
            exit(1)

    def start_and_print_log_still_end(self):
        self.start()
        self.logger.print_while()
        self.communicate()