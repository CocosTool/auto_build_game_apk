import platform
import time
import fileinput
import subprocess
import os
import sys
import _thread
import time
import tail
import json

def get_new_json(filepath, json_new):
    with open(filepath, 'rb') as f:
        json_data = json.load(f)
        for key in json_new.keys():
            json_data[key] = json_new[key]
    f.close()
    return json_data


def rewrite_json_file(filepath, json_data):
    with open(filepath, 'w') as f:
        json.dump(json_data, f)
    f.close()


def updateApkName(ci_params):
    apk_name = r'release-ylcy.apk'
    time_format = time.strftime('%m%d_%H%M', time.localtime(time.time()))
    apk_file_name = ci_params.game_name + '_' + ci_params.gameConfig_client_version + '_' + \
    ci_params.client_version_code + '_' + time_format + '_' + ci_params.outPut_suffix
    if ci_params.build_apk_dev != 'false':
        apk_name = apk_file_name+'_dev.apk'

    if ci_params.build_apk_res != 'false':
        apk_name = apk_file_name+'_release.apk'

    ci_params.apk_name = apk_file_name
    json_data = {
        "apk_name": apk_name,
    }
    current_path = os.path.abspath(".")
    path = os.path.join(current_path, "config/config.json")
    data = get_new_json(path, json_data)
    rewrite_json_file(path, data)


class CIParams(object):
    def __init__(self):
        self.ci_runner_tags = 'unkown'

        self.log_tag = time.strftime("%Y_%m_%d__%H_%M_%S", time.localtime())

        self.root_dir = r'D:/Code/auto_pack/cocos_example'  #替换目标打包工程总目录，目录下包含client目录存放客户端代码，并关联git
        self.cocos_exe_dir = r'D:/Program/CocosCreator_'  #替换成Runner机器上CocosCreator.exe的母目录，为了支持多版本cocos creator
        self.git_dir = r'git@gitlab.com:CocosTool/cocos_example.git'  #客户端git地址

        if "CI_RUNNER_TAGS" in os.environ:
            self.ci_runner_tags = os.environ["CI_RUNNER_TAGS"]

        self.ci_pipeline_id = ''
        if 'CI_PIPELINE_ID' in os.environ:
            self.ci_pipeline_id = os.environ["CI_PIPELINE_ID"]
        
        self.branch_name = r'master'
        if "branch" in os.environ:
            self.branch_name = os.environ["branch"]

        self.client_version = r'unkown'
        if "client_version" in os.environ:
            self.client_version = os.environ["client_version"]
        
        self.client_version_code = r'0'
        if "client_version_code" in os.environ:
            self.client_version_code = os.environ["client_version_code"]
        
        self.client_packab = r'all'
        if "client_packab" in os.environ:
            self.client_packab = os.environ["client_packab"]
        
        self.client_apk_splitab = r'false'#-splitAB
        if "client_apk_splitab" in os.environ:
            self.client_apk_splitab = os.environ["client_apk_splitab"]
        
        self.ab_offset = r'true'#-splitAB
        if "ab_offset" in os.environ:
            self.ab_offset = os.environ["ab_offset"]

        self.is_debug = r'false'
        if "is_debug" in os.environ:
            self.is_debug = os.environ["is_debug"]

        self.dingding_open = r'true'
        if "dingding_open" in os.environ:
            self.dingding_open = os.environ["dingding_open"]

        self.build_exe = r'false'
        if "build_exe" in os.environ:
            self.build_exe = os.environ["build_exe"]

        self.build_apk_dev = r'false'
        if "build_apk_dev" in os.environ:
            self.build_apk_dev = os.environ["build_apk_dev"]

        self.build_apk_res = r'false'
        if "build_apk_res" in os.environ:
            self.build_apk_res = os.environ["build_apk_res"]

        self.build_apk = self.build_apk_dev != 'false' or self.build_apk_res != 'false' or self.build_apk_obb != 'false'

        self.cocos_version_code = r'0'
        if "cocos_version_code" in os.environ:
            self.cocos_version_code = os.environ["cocos_version_code"]

        self.game_config_path = r'/client/assets/resources/game_config/game_config.json'
        if "game_config_path" in os.environ:
            self.game_config_path = os.environ["game_config_path"]

        self.hotupdate_config_path = r'/client/PluginDir/hotUpdate/cfg.json'
        if "hotupdate_config_path" in os.environ:
            self.hotupdate_config_path = os.environ["hotupdate_config_path"]

        self.hotupdate_packageUrl = r'https://cdn.wxgame.youxi765.com/ylcy/remote_assets'
        if "hotupdate_packageUrl" in os.environ:
            self.hotupdate_packageUrl = os.environ["hotupdate_packageUrl"]

        self.hotupdate_packageVersion = r'2.0.0.7'
        if "hotupdate_packageVersion" in os.environ:
            self.hotupdate_packageVersion = os.environ["hotupdate_packageVersion"]

        self.outPut_dir = self.root_dir + os.sep + 'apk'
        if "outPut_dir" in os.environ:
            self.outPut_dir = os.environ["outPut_dir"]

        self.outPut_suffix = ''
        if "outPut_suffix" in os.environ:
            self.outPut_suffix = os.environ["outPut_suffix"]

        self.is_skip_u8 = r'false'
        if "is_skip_u8" in os.environ:
            self.is_skip_u8 = os.environ["is_skip_u8"]

        self.gameConfig_client_version = r'124011015001'
        if "gameConfig_client_version" in os.environ:
            self.gameConfig_client_version = os.environ["gameConfig_client_version"]
        
        self.game_name = r'hello-world'
        if "game_name" in os.environ:
            self.game_name = os.environ["game_name"]

        self.log_dir = self.root_dir + os.sep +'auto_build_log'

        self.apk_name = r'hello-world-release.apk'
        if "apk_name" in os.environ:
            self.apk_name = os.environ["apk_name"]
            
        self.loadApkName()
       

    def loadApkName(self):
        current_path = os.path.abspath(".")
        path = os.path.join(current_path, "config/config.json")
        data = get_new_json(path,{})
        if "apk_name" in data:
            self.apk_name = data["apk_name"]

