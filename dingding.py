#!/usr/bin/env python
#coding=utf-8

import platform
import time
import fileinput
import subprocess
import os
import sys
import _thread
import time
import tail
import json
import asyncprocess

dingding_url1 = 'https://oapi.dingtalk.com/robot/send?access_token=替换token'
dingding_url2 = 'https://oapi.dingtalk.com/robot/send?access_token=替换token'

def sendMessage(cmd_logger, content, isAtAll = False, targetGroup = 1):
    global dingding_url1, dingding_url2
    if targetGroup == 2:
        sendMessageImpl(cmd_logger, dingding_url2, content, isAtAll)
    elif targetGroup == 1:
        sendMessageImpl(cmd_logger, dingding_url1, content, isAtAll)
    else:
        sendMessageImpl(cmd_logger, dingding_url1, content, isAtAll)
        sendMessageImpl(cmd_logger, dingding_url2, content, isAtAll)

def sendMessageImpl(cmd_logger, url, content, isAtAll = False):
    msg_data = {'msgtype':'text','text':{'content':content},'at':{'isAtAll':isAtAll}}

    json_str = json.dumps(msg_data)

    temp_cmd = ['curl', '-H', 'Content-Type: application/json;charset=utf-8', '-X'
                 , 'POST', '-d', json_str, url]

    process = asyncprocess.AsyncProcess(temp_cmd, logger=cmd_logger)
    process.start_and_print_log_still_end()

def sendMarkdown(cmd_logger, title, content, isAtAll = False, targetGroup = 1):
    global dingding_url1, dingding_url2
    if targetGroup == 2:
        sendMarkdownImpl(cmd_logger, dingding_url2, title, content, isAtAll)
    elif targetGroup == 1:
        sendMarkdownImpl(cmd_logger, dingding_url1, title, content, isAtAll)
    else:
        sendMarkdownImpl(cmd_logger, dingding_url1, title, content, isAtAll)
        sendMarkdownImpl(cmd_logger, dingding_url2, title, content, isAtAll)

def sendMarkdownImpl(cmd_logger, url, title, content, isAtAll = False):
    msg_data = {'msgtype':'markdown','markdown':{'text':content,'title':title},'at':{'isAtAll':isAtAll}}

    json_str = json.dumps(msg_data)

    temp_cmd = ['curl', '-H', 'Content-Type: application/json;charset=utf-8', '-X'
                 , 'POST', '-d', json_str, url]

    process = asyncprocess.AsyncProcess(temp_cmd, logger=cmd_logger)
    process.start_and_print_log_still_end()