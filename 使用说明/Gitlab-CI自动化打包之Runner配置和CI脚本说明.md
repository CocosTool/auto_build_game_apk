### **Gitlab-CI自动化CocosCreator安卓打包之Runner配置和CI脚本说明**
#### 一. 新建打包工程
在gitlab上新建一个打包工程目录，比如auto_build_game_apk(https://gitlab.com/CocosTool/auto_build_game_apk)，必须获取工程的master权限。

示例Cocos Hello World工程:[https://gitlab.com/CocosTool/cocos_example](https://gitlab.com/CocosTool/cocos_example)
#### 二. Runner配置

1. 安装Runner

    https://docs.gitlab.com/runner/install/
    下载对应平台源文件,我这里以win10为例。
    
    在任意位置，新建文件夹GitLab-Runner，将gitlab-runner-windows-386.exe放入重命名gitlab-runner.exe，然后命令行执行安装。
    ```shell
    cd C:\GitLab-Runner
    //无用户安装，由于此时运行runner时使用的并不是当前系统账号，后面使用git的时候会有权限问题，不推荐
    ./gitlab-runner.exe install

    //以当前管理员账号安装
    ./gitlab-runner.exe install --user ENTER-YOUR-USERNAME --password ENTER-YOUR-PASSWORD
    //如果提示账号不存在，尝试添加.\
    ./gitlab-runner.exe install --user ".\ENTER-YOUR-USERNAME" --password "ENTER-YOUR-PASSWORD"

    //启动runner
    ./gitlab-runner.exe start

    //停止runner
    ./gitlab-runner.exe stop
    ```
2. 注册Runner

    gitlab的工程界面，进入Settings => CI/CD => Runners settings 获取相关参数，如图：
    ![img](./pictures/runner-register-var.png)

    运行以下命令：
    ```shell
    gitlab-runner register
    ```
    输入您的GitLab实例URL：
    ```shell
    Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
    https://gitlab.com/
    ```
    输入您获得的令牌以注册Runner：
    ```shell
    Please enter the gitlab-ci token for this runner
    4G257zVcnRJzxjGd3evs
    ```
    输入Runner的描述，您可以稍后在GitLab的UI中更改：
    ```shell
    Please enter the gitlab-ci description for this runner
    my-runner
    ```
    输入与Runner关联的标签，您可以稍后在GitLab的UI中更改：
    ```shell
    Please enter the gitlab-ci tags for this runner (comma separated):
    my-tag
    ```
    输入Runner执行程序：
    ```shell
    Please enter the executor: parallels, shell, kubernetes, custom, docker-ssh, ssh, virtualbox, docker+machine, docker-ssh+machine, docker, docker-windows:
    shell
    ```
    注册完成后可在gitlab页面看到一个新的runner：
    ![img](./pictures/runner-state.png)
    我这边是将机器的tag命名为my-tag，这个后面写CI脚本时指定Runner需要用到。

    如果启动时遇到FATAL: Failed to start gitlab-runner: The service did not start due to a logon failure.按以下步骤可解决：
    计算机管理 => 服务 => gitlab-runner =>登录 重新输入密码即可

#### 三. CI/CD脚本编写

新建脚本文件.gitlab-ci.yml:
```yaml
stages:
- prepare
- build
- notice

prepare_out:
  stage: prepare
  script:
  - python ./prepare.py
  only:
  - web
  - triggers
  artifacts:
    paths:
      - config/
    when: on_success
  tags:
  - my-tag

build_apk_cocos:
  stage: build
  script:
  - python ./build_cocos.py
  only:
    variables:
      - $build_apk == "true"
  tags:
  - my-tag

notice_job:
  stage: notice
  script:
  - python ./notice.py
  only:
  - web
  - triggers
  tags:
  - my-tag
```

1. my-tag 替换成你自己的Runner的tag。
2. only下面的配置表示只有在gitlab网页操作和Pipeline triggers时才会触发
```yaml
only:
  - web
  - triggers
```
3. 配置Pipeline triggers：
   gitlab的工程界面，进入Settings => CI/CD => Pipeline triggers 配置相关参数
   Add trigger 然后会获取一个token，页面下方会有具体请求的URL，variables用来传参数给执行runner的环境变量如：
   ```
   curl -X POST \
     -F token=TOKEN \
     -F "ref=REF_NAME" \
     -F "variables[RUN_NIGHTLY_BUILD]=true" \
     https://gitlab.com/api/v4/projects/15418684/trigger/pipeline
   ```
   以cocos_example为例，用PostMan进行测试：
    ```
    Post请求Url:
        https://gitlab.com/api/v4/projects/15418684/trigger/pipeline
    Body:
        token:437ad5074f67b38e5b97699f004a56
        ref:master
        variables[branch]:master
        variables[client_version]:1
        variables[client_version_code]:100
        variables[dingding_open]:true
        variables[build_apk_res]:false
        variables[build_apk_dev]:true
        variables[cocos_version_code]:212
        variables[build_apk]:true
        variables[hotupdate_packageUrl]:https://cdn.wxgame.youxi765.com/ylcy/remote_assets
        variables[hotupdate_packageVersion]:2.0.0.8
        variables[outPut_suffix]:patch_2008
        variables[is_skip_u8]:true
        variables[gameConfig_client_version]:200011018002
        variables[outPut_dir]:D:/outPutDir/apk
    ```

    各个参数说明：
    ```
    branch:要打包apk的分支
    client_version:apk版本名
    client_version_code：apk版本号
    dingding_open:是否打开钉钉通知
    build_apk_res:是否连接正式服
    build_apk_dev:是否连接测试服
    cocos_version_code:cocos creator的版本
    build_apk:是否构建apk
    hotupdate_packageUrl:热更新地址
    hotupdate_packageVersion:热更新版本号
    outPut_suffix:生成的apk命名的后缀添加
    is_skip_u8:是否跳过U8，登录广告等接口默认成功
    gameConfig_client_version:游戏客户端版本号
    outPut_dir:apk输出路径

    //以下可不传，用默认的配置
    game_name:游戏名字
    game_config_path:游戏配置文件的路径
    hotupdate_config_path:热更新文件的路径
    ```

4. 修改Python脚本
    ciparams.py文件:
    ```python
    修改为当前目标打包工程路径
    self.root_dir = r'H:/Code/ylcy'

    修改游戏配置文件的路径
    self.game_config_path = r'/client/assets/resources/game_config/game_config.json'
    if "game_config_path" in os.environ:
        self.game_config_path = os.environ["game_config_path"]

    修改游戏热更新文件的路径
    self.hotupdate_config_path = r'/client/PluginDir/hotUpdate/cfg.json'
    if "hotupdate_config_path" in os.environ:
        self.hotupdate_config_path = os.environ["hotupdate_config_path"]
    ```

    game_config的修改需要游戏自己具体实现，cocos_example中已经实现的修改配置
    ```json
    {
        "version":124011015001,
        "is_dev":true,
        "is_skip_u8":true
    }
    ```
    hotupdate_config_path依赖cocos的构建完自动替换热更新文件的插件.

5. 修改安卓工程
   
   修改gradle脚本支持修改Apk的versionCode,versionName,文件名字及Apk输出路径

   proj.android-studio => app => build.gradle
   ```gradle
   defaultConfig {
        if (project.hasProperty('VERSION_CODE_PARA')) {
            versionCode Integer.parseInt(VERSION_CODE_PARA)
        }
        else{
            versionCode 1
        }
        if (project.hasProperty('VERSION_NAME_PARA')) {
            versionName VERSION_NAME_PARA
        }
        else{
            versionName "1.0.0"
        }
   }

   buildTypes {
        release {
            android.applicationVariants.all { variant ->
                variant.outputs.all { output ->
                    def outputFile = output.outputFile
                    if (outputFile != null && outputFile.name.endsWith('.apk')) {
                        def fileName = outputFile.name;
                        if (project.hasProperty('OUT_PUT_APK_FILE_NAME')) {
                            fileName = "${OUT_PUT_APK_FILE_NAME}";
                        }
                        if (project.hasProperty('OUT_PUT_DIR_PARA')) {
                            File output_dir1 = file("${OUT_PUT_DIR_PARA}");

                            println "输出文件位置： " + output_dir1
                            variant.getPackageApplication().outputDirectory = output_dir1
                            outputFileName = fileName;
                            println "输出文件位置： " + output.outputFile
                        } else {
                            outputFileName = fileName
                            println "输出文件位置： " + output.outputFile
                        }
                    }
                }
            }
        }
   ```

6. 开始正式打包
   
    在Runner的机子上，先手动构建一次Cocos Creator工程，将打包相关配置都配置好，确认Android studio能生成正常的Apk.
    用PostMan抛送Post请求，触发Pipeline trigger,开始自动构建
    notice_job 用来触发打包完成通知
    由于打包完成后包在本地，所以需要在本机上建了一个web服务，支持指定目录的文件下载。参考[Win10搭建web服务实现文件共享](http://www.solves.com.cn/it/wlyx/fwq/2019-05-31/602.html "超链接title")

    修改notice.py文件:
    ```python
    if ci_params.dingding_open != 'false':
        apk_name = ci_params.apk_name

        msg = '#### 打包完成 '
        if ci_params.build_apk != 'false':
            root_url = 'YOUR_WEB_DOWNLOAD_URL'#替换下载地址,例如root_url = 'http://10.0.19.61/apk/'
            if ci_params.build_apk_dev != 'false':
                msg += '\n> 安卓_连测试服包:'+apk_name+'[点击下载](' + root_url + apk_name + ')'
            if ci_params.build_apk_res != 'false':
                msg += '\n\n> 安卓_连正式服包:'+apk_name+'[点击下载](' + root_url + apk_name + ')'

        dingding.sendMarkdown(cmd_logger, '打包完成，抓紧测试', msg, True)
    ```

    修改dingding.py文件:
        dingding_url1，dingding_url2替换成对应游戏打包群的地址