#!/usr/bin/env python
#coding=utf-8

import platform
import time
import fileinput
import subprocess
import os
import sys
import _thread
import time
import tail
import json
import dingding
import ciparams

if __name__ == '__main__':
    ci_params = ciparams.CIParams()

    if ci_params.ci_runner_tags != 'my-tag':
        exit(0)

    cmd_logger = tail.OutErrLogHelper(ci_params.log_dir + os.sep + 'notice_cmd')

    if ci_params.dingding_open != 'false':
        apk_name = ci_params.apk_name

        msg = '#### 打包完成 '
        if ci_params.build_apk != 'false':
            root_url = 'http://10.0.19.61/apk/'
            if ci_params.build_apk_dev != 'false':
                msg += '\n> 安卓_连测试服包:'+apk_name+'[点击下载](' + root_url + apk_name + ')'
            if ci_params.build_apk_res != 'false':
                msg += '\n\n> 安卓_连正式服包:'+apk_name+'[点击下载](' + root_url + apk_name + ')'
                
        dingding.sendMarkdown(cmd_logger, '打包完成，抓紧测试', msg, True)

    cmd_logger.stop()

    
