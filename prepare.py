#!/usr/bin/env python
#coding=utf-8
# encoding: utf-8

import platform
import time
import fileinput
import subprocess
import os
import sys
import _thread
import time
import tail
import shutil
import dingding
import ciparams
import asyncprocess
import json

def cleanProjDir(dir_path, git_url, ci_params, cmd_logger):
    branch_name = ci_params.branch_name
    if not os.path.exists(dir_path):
        process = asyncprocess.AsyncProcess('git clone ' + git_url + ' ' + dir_path, logger=cmd_logger)
        process.start_and_print_log_still_end()
    elif ci_params.client_packab == 'all':
        process = asyncprocess.AsyncProcess('git clean -fd', cwd=dir_path, logger=cmd_logger)
        process.start_and_print_log_still_end()
        
        process = asyncprocess.AsyncProcess('git reset --hard', cwd=dir_path, logger=cmd_logger)
        process.start_and_print_log_still_end()
    
    process = asyncprocess.AsyncProcess('git pull origin '+ branch_name, cwd=dir_path, logger=cmd_logger)
    process.start_and_print_log_still_end()
    
    if ci_params.client_packab == 'all':
        process = asyncprocess.AsyncProcess('git checkout ' + branch_name, cwd=dir_path, logger=cmd_logger)
        process.start_and_print_log_still_end()

def modifyGameConfig(dir_path,ci_params, cmd_logger):
    game_config_path = dir_path+ci_params.game_config_path
    if not os.path.exists(game_config_path):
        cmd_logger.stop()
        print(game_config_path + ' not exists', flush=True)
        exit(1)
    else:
        json_data = {
            "is_dev" : ci_params.build_apk_dev,
            "is_skip_u8" : ci_params.is_skip_u8,
            "version" : ci_params.gameConfig_client_version
        }
        ret_json_data = ciparams.get_new_json(game_config_path,json_data)
        ciparams.rewrite_json_file(game_config_path,ret_json_data)

        if ci_params.dingding_open != 'false':
            dingding.sendMarkdown(cmd_logger, '修改游戏配置文件','#### 修改游戏配置文件\n> 文件路径:'+ game_config_path + ' modify game config success!',False)
            dingding.sendMarkdown(cmd_logger, '修改详情', '#### 修改详情\n> '+ json.dumps(json_data, sort_keys=True, indent=4, separators=(', ', ': ')), False)

def modifyHotUpdateConfig(dir_path,ci_params, cmd_logger):
    hotupdate_config_path = dir_path+ci_params.hotupdate_config_path
    if not os.path.exists(hotupdate_config_path):
        print(hotupdate_config_path + ' not exists,skip change hotupdate config', flush=True)
    else:
        json_data = {
            "packageUrl" : ci_params.hotupdate_packageUrl,
            "autoPackVersion" : ci_params.hotupdate_packageVersion,
        }
        ret_json_data = ciparams.get_new_json(hotupdate_config_path,json_data)
        ciparams.rewrite_json_file(hotupdate_config_path,ret_json_data)

    if ci_params.dingding_open != 'false':
        dingding.sendMarkdown(cmd_logger, '修改热更新配置文件', '#### 修改热更新配置文件\n> 文件路径:'+hotupdate_config_path + ' modify hotupdate config success!', False)
        dingding.sendMarkdown(cmd_logger, '修改详情',
                              '#### 修改详情\n> '+ json.dumps(json_data, sort_keys=True, indent=4, separators=(', ', ': ')), False)


if __name__ == '__main__':
    ci_params = ciparams.CIParams()
    ciparams.updateApkName(ci_params)
    if not os.path.exists(ci_params.root_dir):
            os.makedirs(ci_params.root_dir)

    log_dir = ci_params.log_dir
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    
    cmd_logger = tail.OutErrLogHelper(ci_params.log_dir + os.sep + 'prepare_cmd')

    if ci_params.ci_runner_tags == 'my-tag':
        if ci_params.dingding_open != 'false':
            msg = '#### 我开始打包咯\n> 正在发布'
            if ci_params.build_exe != 'false':
                msg += 'Windows包、'
            if ci_params.build_apk != 'false':
                msg += '安卓('
                split_flag = ""
                if ci_params.build_apk_dev != 'false':
                    msg += '测试包'
                    split_flag = '、'
                if ci_params.build_apk_res != 'false':
                    msg += split_flag + '正式包'
                    split_flag = '、'
                msg += ')'
            msg += '\n> 来源分支名:' + ci_params.branch_name
            msg += '\n> 安卓版本号:' + ci_params.client_version
            if ci_params.build_apk != 'false':
                msg += '\n> 安卓VersionCode:' + ci_params.client_version_code
            msg += '\n\n> 客户端资源打包:'
            if ci_params.client_packab == "all":
                msg += '全部重新打，请耐心等待'
            elif ci_params.client_packab == "none":
                msg += '不再重新打'
            else:
                msg += '重新生成流文件'
            msg += '\n\n> [查看进度](http://git.joyport.inout/yingluo/ylcy_client_auto_build/pipelines/' + ci_params.ci_pipeline_id + ')'
            dingding.sendMarkdown(cmd_logger, '开始打包了', msg, False)

    if ci_params.ci_runner_tags == 'my-tag':
        if ci_params.build_apk == 'false':
            cmd_logger.stop()
            exit(0)
    
    cleanProjDir(ci_params.root_dir + os.sep + 'client', ci_params.git_dir, ci_params, cmd_logger)
    
    modifyGameConfig(ci_params.root_dir,ci_params,cmd_logger)
    modifyHotUpdateConfig(ci_params.root_dir,ci_params,cmd_logger)
    cmd_logger.stop()